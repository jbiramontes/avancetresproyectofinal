package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

import java.util.Objects;

public class Catalogo {
    /**
     * Atributos clase Catalogo
     */
    private Usuario usuarioCatalogo = new Usuario();
    private Cancion cancionCatalogoUsuario = new Cancion();

    /**
     * Usuario relacionado al catalogo
     * @return retorna objeto de clase Usuario
     */
    public Usuario getUsuarioCatalogo() {
        return usuarioCatalogo;
    }

    /**
     * Set para el usuario del catalogo
     * @param usuarioCatalogo usuario del catalogo
     */
    public void setUsuarioCatalogo(Usuario usuarioCatalogo) {
        this.usuarioCatalogo = usuarioCatalogo;
    }

    /**
     * Cancion relacionada al catalogo
     * @return objeto Cancion del catalogo
     */
    public Cancion getCancionCatalogoUsuario() {
        return cancionCatalogoUsuario;
    }

    /**
     * Set para las canciones en el catalogo
     * @param cancionCatalogoUsuario canciones para catalogo
     */
    public void setCancionCatalogoUsuario(Cancion cancionCatalogoUsuario) {
        this.cancionCatalogoUsuario = cancionCatalogoUsuario;
    }

    /**
     * Constructor sin parametros
     */
    public Catalogo() {
    }

    /**
     * Constructor con parametros
     * @param usuarioCatalogo usuario del catalogo
     * @param cancionCatalogoUsuario cancion del catalogo
     */
    public Catalogo(Usuario usuarioCatalogo, Cancion cancionCatalogoUsuario) {
        this.usuarioCatalogo = usuarioCatalogo;
        this.cancionCatalogoUsuario = cancionCatalogoUsuario;
    }

    /**
     * Metodo toString
     * @return retorna contendido de catalogo
     */
    @Override
    public String toString() {
        return "Catalogo{" +
                "usuarioCatalogo=" + usuarioCatalogo +
                ", cancionCatalogoUsuario=" + cancionCatalogoUsuario +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Catalogo)) return false;
        Catalogo catalogo = (Catalogo) o;
        return Objects.equals(usuarioCatalogo, catalogo.usuarioCatalogo) &&
                Objects.equals(cancionCatalogoUsuario, catalogo.cancionCatalogoUsuario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usuarioCatalogo, cancionCatalogoUsuario);
    }
}
