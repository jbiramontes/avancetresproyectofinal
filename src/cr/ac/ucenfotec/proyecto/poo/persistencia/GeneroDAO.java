package cr.ac.ucenfotec.proyecto.poo.persistencia;


import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Album;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Genero;


import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GeneroDAO {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tgenero (nombre,descripcion)" + "values (?,?)";
    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tgenero";
    private PreparedStatement comandoConsulta;

    public GeneroDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Genero nuevoGenero) throws SQLException {

        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoGenero.getNombre());
            this.comandoInsertar.setString(2, nuevoGenero.getDescripcionGenero());
            this.comandoInsertar.execute();
        }
    }

    public List<Object> obtenerGeneros() {
        ArrayList<Object> listaGeneros = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Genero nuevoGenero = new Genero();
                nuevoGenero.setNombre(resultSet.getString("nombre"));
                nuevoGenero.setDescripcionGenero(resultSet.getString("descripcion"));
                listaGeneros.add(nuevoGenero);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaGeneros;
    }
}
