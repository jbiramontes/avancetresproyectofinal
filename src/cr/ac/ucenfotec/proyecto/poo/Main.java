
package cr.ac.ucenfotec.proyecto.poo;

import cr.ac.ucenfotec.proyecto.poo.bl.logica.Gestor;
import cr.ac.ucenfotec.proyecto.poo.controlador.Controlador;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.commons.mail.EmailException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Esta es la clase principal
 *
 * @author: Jorge Biramontes Calvo
 * @version: 1.45
 */

public class Main extends Application {
    static Connection cnx;

    @Override
    public void start(Stage primaryStage) throws Exception {


       // Parent root = FXMLLoader.load(getClass().getResource("RegistroCompositor.fxml"));
        //Parent root = FXMLLoader.load(getClass().getResource("MenuUsuario.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("iufx/InicioSesion.fxml"));
        Stage ventana = primaryStage;

        ventana.setTitle("El Baul de los recuerdos");
        ventana.setScene(new Scene(root, 606, 377));
        ventana.show();

    }


    public static void main(String[] args) throws SQLException, EmailException {
       launch(args);
       // Controlador controlador = new Controlador();
      // controlador.ejecutarPrograma();
        //menu.idCanciones();
       // Gestor gestor=new Gestor();
       //controlador.inicioSesion("jorgebc","123");
        //gestor.comprobarCredenciales("cd","cd");

        //menu.listarUsuarios();


    }


}

