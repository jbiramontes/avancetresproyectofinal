package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ControllerRegistroCancion {
    Controlador controlador = new Controlador();
    @FXML
    private Button btnRegistrar;

    @FXML
    private Button atras;

    @FXML
    private TextField nombreCancion;

    @FXML
    private TextField nombreArtista;

    @FXML
    private TextField calificacion;

    @FXML
    private TextField nombreCompositor;

    @FXML
    private TextField nombreGenero;

    @FXML
    void retroceder(ActionEvent event) {
        new ToScene().toScene("MenuUsuario.fxml", event);
    }

    @FXML
    void registroCancion(ActionEvent event) {
        controlador.registrarCancion(nombreCancion.getText(),calificacion.getText(),nombreArtista.getText(),nombreGenero.getText(),nombreCompositor.getText());
    }
}
