package cr.ac.ucenfotec.proyecto.poo.controlador;

import cr.ac.ucenfotec.proyecto.poo.bl.logica.Gestor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javax.swing.*;
import java.util.List;

public class ControllerMenuUsuario {
    Gestor gestor = new Gestor();
    Controlador controlador = new Controlador();
    @FXML
    private Button btnCrearAlbum;
    @FXML
    private Label mensajeBienvenida;
    @FXML
    private Button btnMostarListas;
    @FXML
    private Button registroCancion;
    @FXML
    private Button salir;

    @FXML
    void registrarCancion(ActionEvent event) {
        new ToScene().toScene("RegistroCancion.fxml", event);
    }

    @FXML
    void salir(ActionEvent event) {
        gestor.salirMenus(salir);
    }

    @FXML
    void crearAlbum(ActionEvent event) {
        //controlador.crearAlbum();
        JOptionPane.showMessageDialog(null, "Opcion aun no habilitada");
    }

    @FXML
    void crearLista(ActionEvent event) {
        controlador.crearListaReproduccion();
    }

    @FXML
    void mostrarlistas(ActionEvent event) {
        List<Object> lista=gestor.listar(9);
        for (Object a: lista){
            System.out.println(a);

        }
    }


}
