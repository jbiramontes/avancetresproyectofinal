package cr.ac.ucenfotec.proyecto.poo.bl.logica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;


import cr.ac.ucenfotec.proyecto.poo.PropertiesHandler;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.*;
import cr.ac.ucenfotec.proyecto.poo.persistencia.*;

import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class Gestor {

    private AlbumDAO albumDAO;
    private UsuarioDao usuarioDao;
    private ArtistaDAO artistaDAO;
    private CancionDao cancionDao;
    private CatalogoDAO catalogoDAO;
    private CompositorDAO compositorDAO;
    private GeneroDAO generoDAO;
    private ListaReproduccionDAO listaReproduccionDAO;


    private Connection conection;
    PropertiesHandler newPropertie = new PropertiesHandler();


    public Gestor() {
        try {
            newPropertie.loadProperties();
            Class.forName(newPropertie.getDriver()).newInstance();
            this.conection = DriverManager.getConnection(newPropertie.getCnxStr(), newPropertie.getUsrname(),
                    newPropertie.getPassword());
            this.usuarioDao = new UsuarioDao(this.conection);
            this.albumDAO = new AlbumDAO(this.conection);
            this.artistaDAO = new ArtistaDAO(this.conection);
            this.cancionDao = new CancionDao(this.conection);
            //this.catalogoDAO = new CatalogoDAO(this.conection);
            this.compositorDAO = new CompositorDAO(this.conection);
            this.generoDAO = new GeneroDAO(this.conection);
            this.listaReproduccionDAO = new ListaReproduccionDAO(this.conection);

        } catch (Exception e) {
            System.out.println("Cant connect to db");
            System.out.println(e.getMessage());
        }
    }

    /**
     * Metodo para pasar un String a un LocalDate
     *
     * @param fecha recibe este parametro como un String
     * @return LocalDate
     */
    public LocalDate convertirFecha(String fecha) {
        try {
            return LocalDate.parse(fecha);
        } catch (Exception e) {
            System.out.println("Usted ha digitado la fecha en un formato incorrecto");

        }
        return null;
    }

    /**
     * Metodo para calculo de edad
     *
     * @param fechaNacimiento ingresado por usuario
     * @return la cantidad de años segun fecha nacimiento
     */
    public int calcularEdad(LocalDate fechaNacimiento) {
        LocalDate actual = LocalDate.now();
        Period periodo = Period.between(fechaNacimiento, actual);
        return periodo.getYears();
    }

    /**
     * Metodo para envio de correos
     *
     * @param correo recibe correo de usuario
     * @param nombre recibe nombre de usuario
     * @throws EmailException manejo de errores
     */
    public void enviarCorreo(String correo, String nombre) throws EmailException {
        try {
            Email email = new SimpleEmail();
            email.setSmtpPort(587);
            email.setAuthenticator(new DefaultAuthenticator("jbircalvo@gmail.com", "JORbir1980"));
            email.setDebug(false);
            email.setHostName("smtp.gmail.com");
            email.setFrom("jbircalvo@gmail.com");
            email.setSubject("Codigo verificacion El Baul de los Recuerdos");
            email.setMsg("Hola " + nombre + " su codigo de verificacion es 236571");
            email.addTo(correo);
            email.setTLS(true);
            email.send();
        } catch (EmailException e) {
            System.out.println("El correo no pudo ser enviado");
        }
    }


    public int comprobarCredenciales(String usuario, String contrasena) {
        int contador = 0;
        List<Usuario> credenciales;
        credenciales = usuarioDao.obtenerCredenciales();
        for (Usuario a : credenciales) {
            if (a.getNombreUsuario().contentEquals(usuario) && a.getContrasena().contentEquals(contrasena)) {
                contador++;
            } else {
            }
        }
        return contador;
    }

    public void salirPrograma() {
        System.exit(0);
    }

    public void salirMenus(Button salir) {
        Stage stage = (Stage) salir.getScene().getWindow();
        stage.close();
        System.exit(0);
    }

    public void agregarAlbum(String nombreAlbum, LocalDate fechaLanzamiento, String imagen, ArrayList canciones) {
        Album nuevoAlbum = new Album(nombreAlbum, fechaLanzamiento, imagen, canciones);
        try {
            albumDAO.save(nuevoAlbum);
        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    public void agregarArtista(String nombre, String apellidoUno, String apellidoDos, LocalDate fechaNacimiento, int edad, String pais,
                               String descripcion, String nombreArtistico, LocalDate fechaDefuncion) {
        Artista nuevoArtista = new Artista(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais, descripcion, nombreArtistico, fechaDefuncion);
        try {
            artistaDAO.save(nuevoArtista);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarCancion(String nombreCancion, int calificacion, String artista, String generoCancion, String compositorCancion) {
        Cancion nuevaCancion = new Cancion(nombreCancion, calificacion, artista, generoCancion, compositorCancion);
        try {
            cancionDao.save(nuevaCancion);

        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarCompositor(String nombre, String apellidoUno, String apellidoDos, LocalDate fechaNacimiento, int edad, String pais) {
        Compositor nuevoCompositor = new Compositor(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais);
        try {
            compositorDAO.save(nuevoCompositor);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public void agregarGenero(String nombre, String descripcionGenero) {
        Genero nuevoGenero = new Genero(nombre, descripcionGenero);
        try {
            generoDAO.save(nuevoGenero);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public boolean agregarUsuario(String nombre, String apellidoUno, String apellidoDos, String fecha, String pais,
                                  String identificacion, String imagen, String correoUsuario, String nombreUsuario, String contrasena, String tipoUsuario) {
        LocalDate fechaNacimiento = convertirFecha(fecha);
        int edad = calcularEdad(fechaNacimiento);
        if (edad >= 18) {
            Usuario nuevoUsuario = new Usuario(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais, identificacion, imagen, correoUsuario, nombreUsuario, contrasena, tipoUsuario);
            try {
                usuarioDao.save(nuevoUsuario);
                enviarCorreo(correoUsuario, nombreUsuario);
                return true;
            } catch (SQLException a) {
                a.printStackTrace();
                return false;
            } catch (EmailException e) {


            }
        }
        return false;
    }

    public void agregarListaReproduccion(String nombreLista, LocalDate fechaCreacion, /*ArrayList<Cancion> cancionesLista,*/ String identificacionUsuario) {
        ListaReproduccion nuevaLista = new ListaReproduccion(nombreLista, fechaCreacion, /*cancionesLista,*/ identificacionUsuario);
        try {
            listaReproduccionDAO.save(nuevaLista);
        } catch (SQLException a) {
            a.printStackTrace();

        }

    }

    public List<Object> listar(int opcion) {

        if (opcion == 4) {
            List<Object> usuarios = usuarioDao.obtenerUsuarios();

            return usuarioDao.obtenerUsuarios();
        }


        if (opcion == 4) {
            List<Object> artistas = artistaDAO.obtenerArtistas();
            return artistas;

        }

        if (opcion == 5) {
            List<Object> albumes = albumDAO.obtenerAlbum();
            return albumes;

        }
        if (opcion == 6) {
            List<Object> compositores = compositorDAO.obtenerUsuarios();
            return compositores;

        }
        if (opcion == 7) {
            List<Object> canciones = cancionDao.obtenerCanciones();
            return canciones;
        }
        if (opcion == 8) {
            List<Object> genero = generoDAO.obtenerGeneros();
            return genero;
        }
        if (opcion == 9) {
            List<Object> listas = listaReproduccionDAO.obtenerListaReproduccion();
            return listas;
        }
        return null;
    }


}