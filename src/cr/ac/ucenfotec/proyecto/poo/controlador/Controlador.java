package cr.ac.ucenfotec.proyecto.poo.controlador;

import cr.ac.ucenfotec.proyecto.poo.bl.entidades.*;
import cr.ac.ucenfotec.proyecto.poo.bl.logica.Gestor;
import cr.ac.ucenfotec.proyecto.poo.iu.IU;
import javafx.event.ActionEvent;
import org.apache.commons.mail.EmailException;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Controlador {
    private IU Interfaz = new IU();
    private Gestor gestor = new Gestor();

    public void ejecutarPrograma() throws EmailException {
        int opcion = 1;
        Interfaz.menuPrincipal();
        opcion = Interfaz.leerOpcionMenu();
        switchMenuPrincipal(opcion);
        if (opcion == 0) {
            gestor.salirPrograma();
        }
    }

    private void switchMenuPrincipal(int opcion) throws EmailException {
        switch (opcion) {
            case 0:
                Interfaz.mensajes("---Te esperamos pronto---");
                gestor.salirPrograma();
                break;
            case 1:
                Interfaz.mensajes("Digite nombre usuario");
                String nombre = Interfaz.leerTexto();
                Interfaz.mensajes("Digite contrasena");
                String contrasena = Interfaz.leerTexto();

                inicioSesion(nombre, contrasena);
                Interfaz.menuInicioUsuario();
                opcion = Interfaz.leerOpcionMenu();
                switchMenuUsuario(opcion);

                break;
            case 2:
                crearUsuario();
                ejecutarPrograma();
                break;
            case 3:
                break;

            default:
                Interfaz.mensajes("Opcion no valida");
        }
    }

    private void switchMenuUsuario(int opcion) {
        switch (opcion) {
            case 0:
                Interfaz.mensajes("---Te esperamos pronto---");
                gestor.salirPrograma();
                break;
            case 1:
                //registrarCancion();
                break;
            case 2:
                crearListaReproduccion();
                break;
            case 3:

                break;
            case 4:
                listar(4);
                break;
            case 5:
                listar(5);
                break;
            case 6:
                listar(6);
                break;
            case 7:
                listar(7);
                break;
            case 8:
                listar(8);
                break;
            case 9:
                listar(9);
                break;
            default:
                Interfaz.mensajes("Opcion no valida");
        }
    }

    private void listar(int opcion) {
        List<Object> listar = gestor.listar(opcion);
        for (Object e : listar) {
            System.out.println(e);
        }
    }

    private void listarSinOpcion() {
    }

    public void crearAlbum() {

    }

    public void crearListaReproduccion() {
        try {
            Interfaz.mensajes("Ingrese los datos que se le solicitan: ");
            Interfaz.mensajes("Nombre de la lista ");
            String nombre = Interfaz.leerTexto();
            LocalDate fechaCreacion = LocalDate.now();
            Interfaz.mensajes("Introduzca su nombre usuario");
            String idUsuario = Interfaz.leerTexto();
            gestor.agregarListaReproduccion(nombre, fechaCreacion, idUsuario);
            System.out.println("Su lista se ha registrado con exito el " + fechaCreacion.toString());
        } catch (Exception e) {
            System.out.println("Error al registrar la lista");

        }

    }

    public ArrayList<Integer> idCanciones() {
        ArrayList<Integer> idCanciones = new ArrayList<>();
        int opcion = 0;
        int numero = 3;
        do {

            Interfaz.mensajes("Ingrese el id de la cancion que desea agregar");
            int idCancion = Interfaz.leerOpcionMenu();
            idCanciones.add(idCancion);
            Interfaz.mensajes("Desea agregar mas canciones digite 0 para salir ");
            opcion = Interfaz.leerOpcionMenu();
            for (Integer e : idCanciones) {
                System.out.println(e);
                if (e == numero) {
                    System.out.println("Son iguales");

                }
            }
        } while (opcion != 0);
        gestor.salirPrograma();
        return idCanciones;

    }

    public void registrarCancion(String nombre, String calificacion, String artista, String genero, String compositor) {
        //crearCancion();
        int calificacionNumero = Integer.parseInt(calificacion);
        gestor.agregarCancion(nombre, calificacionNumero, artista, genero, compositor);
        System.out.println("Exitoso");
    }

    /**
     * Proceso de registro solicita datos registro usuario
     */
    private void crearUsuario() throws EmailException {


        try {
            Interfaz.mensajes("Ingrese los datos que se le solicitan: ");
            Interfaz.mensajes("Nombre  ");
            String nombre = Interfaz.leerTexto();
            Interfaz.mensajes("Primer apellido ");
            String apellidoUno = Interfaz.leerTexto();
            Interfaz.mensajes("Segundo apellido ");
            String apellidoDos = Interfaz.leerTexto();
            Interfaz.mensajes("Fecha nacimiento ");
            String fecha = Interfaz.leerTexto();
            LocalDate fechaNacimiento = gestor.convertirFecha(fecha);
            int edad = gestor.calcularEdad(fechaNacimiento);
            Interfaz.mensajes("Pais ");
            String pais = Interfaz.leerTexto();
            Interfaz.mensajes("Numero de identificacion ");
            String idUsuario = Interfaz.leerTexto();
            Interfaz.mensajes("Imagen ");
            String imagen = Interfaz.leerTexto();
            Interfaz.mensajes("Correo ");
            String correo = Interfaz.leerTexto();
            Interfaz.mensajes("Nombre usuario o Alias ");
            String nombreUsuario = Interfaz.leerTexto();
            Interfaz.mensajes("Contraseña ");
            String contrasena = Interfaz.leerTexto();
            Interfaz.mensajes("Tipo usuario ");
            String tipoUsuario = Interfaz.leerTexto();
            //gestor.agregarUsuario(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais, idUsuario, imagen, correo, nombreUsuario, contrasena, tipoUsuario);
            gestor.enviarCorreo(correo, nombreUsuario);
            Interfaz.mensajes("Registro exitoso");

        } catch (Exception e) {

            System.out.println("Usuario no registrado");

        }
    }

    /**
     * Proceso de registro de canciones
     */
    private void crearCancion() {
        int opcion = 0;
        do {
            try {
                Interfaz.mensajes("Registro de nueva cancion");
                Interfaz.mensajes("Nombre de la cancion ");
                String nombreCancion = Interfaz.leerTexto();
                int calificacion = 6;
                do {
                    Interfaz.mensajes("Calificacion");

                    calificacion = Interfaz.leerOpcionMenu();

                    if (calificacion < 0 || calificacion > 5) {
                        System.out.println("La calificacion es de 0 a 5 solamente");
                    }
                } while (calificacion < 0 || calificacion > 5);

                Interfaz.mensajes("Ingrese el id del artista de la cancion");
                //Aqui falta Codigo para buscar id de artista en base de datos y extraerlo
                System.out.println("23");
                Artista artista = new Artista();
                Interfaz.mensajes("Ingrese el id del genero de la cancion");
                System.out.println("28");
                //Aqui falta Codigo para buscar id de genero en base de datos y extraerlo
                Genero generoCancion = new Genero();
                Interfaz.mensajes("Ingrese el id del compositor");
                System.out.println("89");
                //Aqui falta Codigo para buscar id del compositor en base de datos y extraerlo
                Compositor compositorCancion = new Compositor();

                //gestor.insertarDB(gestor.agregarCancion(nombreCancion, calificacion,
                //       artista, generoCancion, compositorCancion));


            } catch (Exception e) {
                System.out.println("Cancion no se pudo registar revise los datos ingresados");
                System.exit(0);
            }
            System.out.println("Desea registrar otra cancion?" + '\n' +
                    "Digite 0 para salir o cualquier numero para continuar");
            opcion = Interfaz.leerOpcionMenu();
        } while (opcion != 0);

        System.exit(0);

    }

    public void crearArtista() {

        try {
            Interfaz.mensajes("Descripcion del artista ");
            String descripcion = Interfaz.leerTexto();
            Interfaz.leerTexto();
            Interfaz.mensajes("Nombre artistico ");
            String nombreArtistico = Interfaz.leerTexto();
            Interfaz.mensajes("Fecha defuncion(Si aplica) ");
            String fechaDefuncion = Interfaz.leerTexto();
            //gestor.agregarArtista(nuevaPersona, descripcion, nombreArtistico, fechaDefuncion);
            Interfaz.mensajes("Artista registrado correctamente");
        } catch (Exception e) {
            System.out.println("El Artista no pudo ser registrado");
        }

    }


    /**
     * Metodo boton de registro Usuario
     *
     * @param actionEvent llama metodo crearUsuario
     */
    public void registro(ActionEvent actionEvent) throws EmailException {
        crearUsuario();

    }

    public void Salir() {
        JOptionPane.showMessageDialog(null, "Gracias por utilizar nuestra plataforma ");
        System.exit(0);
    }

    public boolean inicioSesion(String usuario, String contrasena) {
        try {
            String opcionNula = "";
            if (contrasena.equals(opcionNula) && usuario.equals(opcionNula)) {
                //JOptionPane.showMessageDialog(null, "No ingreso el usuario o la contrasena intente de nuevo");
                return false;
            }
            if (gestor.comprobarCredenciales(usuario, contrasena) == 0) {
                //System.out.println("Credenciales no validas");
                return false;
            } else {
                //System.out.println("Bienvenido a la aplicacion");
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Usted ha ingresado datos no validos");
        }
        return false;
    }


    public void registrarUsuario(String nombre, String apellidoUno, String apellidoDos, String fechaNacimiento, String pais,
                                 String identificacion, String imagen, String correoUsuario, String nombreUsuario, String contrasena, String tipoUsuario) {
        try {

            boolean respuesta = gestor.agregarUsuario(nombre, apellidoUno, apellidoDos, fechaNacimiento, pais, identificacion, imagen, correoUsuario, nombreUsuario, contrasena, tipoUsuario);
            if (respuesta) {
                JOptionPane.showMessageDialog(null, "Usuario registrado con exito");
                JOptionPane.showMessageDialog(null, "Le hemos enviado un codigo de acceso a su correo");
            } else {
                JOptionPane.showMessageDialog(null, "Usuario no cumple con la edad minima");

            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Usuario no se pudo registrar");
            e.printStackTrace();
        }
    }
}

