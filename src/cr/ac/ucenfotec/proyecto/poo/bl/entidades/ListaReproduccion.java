package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

import java.time.LocalDate;
import java.util.ArrayList;

public class ListaReproduccion {
    private String nombreLista;
    private LocalDate fechaCreacion;
    //private ArrayList<Cancion>cancionesLista=new ArrayList<>();
    private Usuario identificacionUsuario= new Usuario();

    /**
     * Atributos de clase ListaReproduccion
     */
    public String getNombreLista() {
        return nombreLista;
    }

    public void setNombreLista(String nombreLista) {
        this.nombreLista = nombreLista;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /*public ArrayList<Cancion> getCancionesLista() {
        return cancionesLista;
    }

    public void setCancionesLista(ArrayList<Cancion> cancionesLista) {
        this.cancionesLista = cancionesLista;
    }*/

    public String  getIdentificacionUsuario() {
        return identificacionUsuario.getIdentificacion();
    }

    public void setIdentificacionUsuario(String identificacionUsuario) {
        this.identificacionUsuario.setIdentificacion(identificacionUsuario);
    }

    public ListaReproduccion() {
    }

    public ListaReproduccion(String nombreLista, LocalDate fechaCreacion, /*ArrayList<Cancion> cancionesLista,*/ String identificacionUsuario) {
        this.nombreLista = nombreLista;
        this.fechaCreacion = fechaCreacion;
        //this.cancionesLista = cancionesLista;
        this.identificacionUsuario.setIdentificacion(identificacionUsuario);
    }

    @Override
    public String toString() {
        return "ListaReproduccion--" +
                "nombreLista='" + nombreLista + '\'' +
                ", fechaCreacion=" + fechaCreacion +/*
                ", canciones en lista=" + cancionesLista +*/
                ", identificacionUsuario=" + identificacionUsuario +
                ' ';
    }
}
