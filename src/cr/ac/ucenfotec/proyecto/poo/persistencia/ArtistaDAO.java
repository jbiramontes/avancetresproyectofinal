package cr.ac.ucenfotec.proyecto.poo.persistencia;


import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Artista;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ArtistaDAO {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tartistas (descripcion,nombreArtista,fechaDefuncion,nombre,apellidoUno,apellidoDos,edad,pais)" + "values (?,?,?,?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tartistas";
    private PreparedStatement comandoConsulta;


    public ArtistaDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Artista nuevoArtista) throws SQLException {
        LocalDate fecha = nuevoArtista.getFechaNacimiento();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoArtista.getDescripcion());
            this.comandoInsertar.setString(2, nuevoArtista.getNombreArtistico());
            this.comandoInsertar.setDate(3, fechaSql);
            this.comandoInsertar.setString(4, nuevoArtista.getNombre());
            this.comandoInsertar.setString(5, nuevoArtista.getApellidoUno());
            this.comandoInsertar.setString(6, nuevoArtista.getApellidoDos());
            this.comandoInsertar.setInt(7, nuevoArtista.getEdad());
            this.comandoInsertar.setString(8, nuevoArtista.getPais());

            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerArtistas() {
        ArrayList<Object> listaArtistas = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Artista nuevoArtista = new Artista();
                nuevoArtista.setDescripcion(resultSet.getString("descripcion"));
                nuevoArtista.setNombreArtistico(resultSet.getString("nombreArtista"));
                nuevoArtista.setFechaDefuncion(resultSet.getDate("fechaDefuncion").toLocalDate());
                nuevoArtista.setNombre(resultSet.getString("nombre"));
                nuevoArtista.setApellidoUno(resultSet.getString("apellidoUno"));
                nuevoArtista.setApellidoDos(resultSet.getString("apellidoDos"));
                nuevoArtista.setEdad(resultSet.getInt("edad"));
                nuevoArtista.setPais(resultSet.getString("pais"));

             listaArtistas.add(nuevoArtista);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaArtistas;
    }

}

