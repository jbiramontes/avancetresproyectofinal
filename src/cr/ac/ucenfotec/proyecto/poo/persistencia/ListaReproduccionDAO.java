package cr.ac.ucenfotec.proyecto.poo.persistencia;

import cr.ac.ucenfotec.proyecto.poo.PropertiesHandler;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Cancion;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.ListaReproduccion;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ListaReproduccionDAO {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tlistareproduccion (nombreLista,fechaCreacion,idUsuario)" +
            "values (?,?,?)";


    private PreparedStatement comandoInsertar;

    private final String TEMPLATE_CONSULTA = "select * from tlistareproduccion";
    private PreparedStatement comandoConsulta;


    public ListaReproduccionDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(ListaReproduccion nuevaLista) throws SQLException {
        LocalDate fecha = nuevaLista.getFechaCreacion();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevaLista.getNombreLista());
            this.comandoInsertar.setDate(2, fechaSql);
            this.comandoInsertar.setString(3, nuevaLista.getIdentificacionUsuario());
            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerListaReproduccion() {
        ArrayList<Object> listas = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                ListaReproduccion nuevaLista = new ListaReproduccion();
                nuevaLista.setNombreLista(resultSet.getString("nombreLista"));
                nuevaLista.setFechaCreacion(resultSet.getDate("fechaCreacion").toLocalDate());
                nuevaLista.setIdentificacionUsuario(resultSet.getString("idUsuario"));


                listas.add(nuevaLista);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listas;
    }

}

