package cr.ac.ucenfotec.proyecto.poo.persistencia;

import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Cancion;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Genero;

import java.sql.*;

import java.util.ArrayList;
import java.util.List;

public class CancionDao {
    Connection cnx;
    private final String TEMPLATE_INSERTAR = "insert into tcanciones (nombreCancion,calificacion,nombreArtista,nombreCompositor,nombreGenero)" + "values (?,?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from tcanciones";

    private PreparedStatement comandoConsulta;




    public CancionDao(Connection cnx) {
        this.cnx = cnx;
        try {
               this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Cancion nuevaCancion) throws SQLException {

        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevaCancion.getNombreCancion());
            this.comandoInsertar.setInt(2, nuevaCancion.getCalificacion());
            this.comandoInsertar.setString(3, nuevaCancion.getArtista());
            this.comandoInsertar.setString(4, nuevaCancion.getCompositor());
            this.comandoInsertar.setString(5, nuevaCancion.getGeneroCancion());
            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerCanciones() {
        ArrayList<Object> listaCanciones = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {

                Cancion nuevaCancion = new Cancion();
                nuevaCancion.setNombreCancion(resultSet.getString("nombreCancion"));
                nuevaCancion.setArtista(resultSet.getString("nombreArtista"));
                nuevaCancion.setGeneroCancion(resultSet.getString("nombreGenero"));
                nuevaCancion.setCompositor(resultSet.getString("nombreCompositor"));
                nuevaCancion.setCalificacion(resultSet.getInt("calificacion"));


                listaCanciones.add(nuevaCancion);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaCanciones;
    }

}

