package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

public class Cancion {
    /**
     * Atributos de clase Cancion
     */
    private String nombreCancion;
    private int calificacion;
    private Artista artista = new Artista();
    private Genero generoCancion = new Genero();
    private Compositor compositorCancion = new Compositor();


    public String getNombreCancion() {
        return nombreCancion;
    }

    public void setNombreCancion(String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public String getArtista() {
        return artista.getNombreArtistico();
    }

    public void setArtista(String artista) {
        this.artista.setNombreArtistico(artista);
    }

    public String getGeneroCancion() {
        return generoCancion.getNombre();
    }

    public void setGeneroCancion(String generoCancion) {
        this.generoCancion.setNombre(generoCancion);
    }

    public String getCompositor() {
        return compositorCancion.nombre;
    }

    public void setCompositor(String compositorCancion) {
        this.compositorCancion.setNombre(compositorCancion);
    }

    public Cancion() {
    }

    public Cancion(String nombreCancion, int calificacion, String nombreArtista, String nombreGenero, String nombreCompositor) {

        this.nombreCancion = nombreCancion;
        this.calificacion = calificacion;
        this.artista.setNombreArtistico(nombreArtista);
        this.generoCancion.setNombre(nombreGenero);
        this.compositorCancion.setNombre(nombreCompositor);
    }

    @Override
    public String toString() {
        return "Cancion --" +
                ", nombreCancion='" + nombreCancion +
                ", calificacion=" + calificacion +
                ", artista=" + artista.getNombreArtistico() +
                ", generoCancion=" + generoCancion.getNombre() +
                ", compositorCancion=" + compositorCancion.getNombre() +
                " ";
    }
}
