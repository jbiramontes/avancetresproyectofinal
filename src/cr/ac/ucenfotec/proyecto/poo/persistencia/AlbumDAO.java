package cr.ac.ucenfotec.proyecto.poo.persistencia;

import cr.ac.ucenfotec.proyecto.poo.PropertiesHandler;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Album;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Cancion;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AlbumDAO {

    Connection cnx;

    private final String TEMPLATE_INSERTAR = "insert into talbum (nombre,fechaLanzamiento,imagen)" + "values (?,?,?,?)";


    private PreparedStatement comandoInsertar;
    private final String TEMPLATE_CONSULTA = "select * from talbum";
    private PreparedStatement comandoConsulta;


    public AlbumDAO(Connection cnx) {
        this.cnx = cnx;
        try {
            this.comandoConsulta = cnx.prepareStatement(TEMPLATE_CONSULTA);
            this.comandoInsertar = cnx.prepareStatement(TEMPLATE_INSERTAR);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void save(Album nuevoAlbum) throws SQLException {
        LocalDate fecha = nuevoAlbum.getFechaLanzamiento();
        Date fechaSql = Date.valueOf(fecha);
        if (this.comandoInsertar != null) {
            this.comandoInsertar.setString(1, nuevoAlbum.getNombreAlbum());
            this.comandoInsertar.setDate(2, fechaSql);
            this.comandoInsertar.setString(3, nuevoAlbum.getImagen());

            this.comandoInsertar.execute();
        }


    }

    public List<Object> obtenerAlbum() {
        ArrayList<Object> listaAlbumes = new ArrayList<>();
        try {
            ResultSet resultSet = this.comandoConsulta.executeQuery();
            while (resultSet.next()) {
                Album nuevoAlbum = new Album();
                nuevoAlbum.setNombreAlbum(resultSet.getString("nombre"));
                nuevoAlbum.setFechaLanzamiento(resultSet.getDate("apellido_uno").toLocalDate());
                nuevoAlbum.setImagen(resultSet.getString("apellido_dos"));

                listaAlbumes.add(nuevoAlbum);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaAlbumes;
    }

}
