package cr.ac.ucenfotec.proyecto.poo.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class ControllerRegistroUsuario {

    Controlador controlador = new Controlador();
    @FXML
    private TextField fechaNacimiento;

    @FXML
    private TextField edad;

    @FXML
    private TextField nombre;

    @FXML
    private TextField apellidoUno;

    @FXML
    private TextField apellidoDos;

    @FXML
    private Button btnRegistrar;

    @FXML
    private Button atras;

    @FXML
    private TextField nombreUsuario;

    @FXML
    private TextField correo;

    @FXML
    private TextField id;

    @FXML
    private TextField avatar;

    @FXML
    private TextField pais;

    @FXML
    private TextField tipoUsuario;

    @FXML
    private PasswordField password;

    @FXML
    void registroUsuario(ActionEvent event) {
        controlador.registrarUsuario(nombre.getText(), apellidoUno.getText(), apellidoDos.getText(), fechaNacimiento.getText(), pais.getText(), id.getText(), avatar.getText(), correo.getText(), nombreUsuario.getText(), password.getText(), tipoUsuario.getText());
        new ToScene().toScene("InicioSesion.fxml", event);
    }

    @FXML
    void retroceder(ActionEvent event) {
        new ToScene().toScene("InicioSesion.fxml", event);
    }


}
